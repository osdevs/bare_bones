# 'kernel_main' is external, however .extern is ignored by GNU AS, this is just for definition
.extern kernel_main

# declaring 'start' label as global, for the linker
.global start

# GRUB is used as bootloader and requires some fixed values for 'Multiboot' standard
.set MB_MAGIC, 0x1BADB002          # 'magic' constant that GRUB uses to detect kernel's location
.set MB_FLAGS, (1 << 0) | (1 << 1) # (align loaded modules on page boundaries) | (provide a memory map)
.set MB_CHECKSUM, -(MB_MAGIC + MB_FLAGS) # calculate a checksum that includes all the previous values
 

# section for the executable containing Multiboot header
.section .multiboot
	.align 4 # aligning on multiple of 4 bytes
	# use the previously defined constants
	.long MB_MAGIC
	.long MB_FLAGS
	# use the previously calculated checksum
	.long MB_CHECKSUM
 
# section for initialising data to zero
.section .bss
	# stack implementation for C code, 4096 bytes are allocated
	.align 16
	stack_bottom:
		.skip 4096 # reserve a 4096-byte (4KiB) stack
	stack_top:
 
# section for functional code
.section .text
	# 'start' label mentioned earlier, first code to be run
	start:
		# C requires a stack to function
		mov $stack_top, %esp # set the stack pointer to the top of the stack, the stack moves downwards in x86
 
		# calling the main function in C
		call kernel_main
 
		# If, C code returns, hang the CPU
		hang:
			cli      # disable CPU interrupts
			hlt      # halt the CPU
			jmp hang # If that didn't work, loop around and try again
