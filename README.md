# Bare Bones #

This is a simple freestanding kernel written in C and x86 Assembly to display text.

## Table of Contents ##
[TOC]

## Prerequisites ##

* A cross-compiler for _i686-elf_
* A texteditor of some kind

## Structure ##

`start.s` - This is the x86 assembly code to start the kernel and set up the x86  
`kernel.c` - This the kernel  
`linker.ld` - This contains information to link the previous files together to create an executable kernel

## Build ##

### Using Make ###
__This is the preferred method.__

#### To Build ####

```shell
make CROSS_COMPILE=/path/to/cross_compiler/i686-elf-
```
#### To Clean ####

```shell
make clean
```

### Manually ###

#### Assembly ####

```shell
i686-elf-as start.s -o start.o
```

#### Kernel ####

```shell
i686-elf-gcc -c kernel.c -o kernel.o -std=gnu99 -ffreestanding -Wall -Wextra -Werror
```

#### Linking ####

```shell
i686-elf-gcc -ffreestanding -nostdlib -T linker.ld start.o kernel.o -o os.elf -lgcc
```

## Testing ##

### QEMU ###

```shell
qemu-system-i386 -kernel os.elf
```
