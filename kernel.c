// GCC provides these header files automatically
#include <stddef.h>
#include <stdint.h>
 
// VGA textmode buffer of x86, to display text data is written to this memory location
volatile uint16_t* VGA_BUFFER = (uint16_t*)0xB8000;
// By default, the VGA textmode buffer has a size of 80x25 characters
static const size_t VGA_COLS = 80;	// width
static const size_t VGA_ROWS = 25;	// height

enum vga_colour
{
	VGA_COLOUR_BLACK = 0x0,
	VGA_COLOUR_BLUE = 0x1,
	VGA_COLOUR_GREEN = 0x2,
	VGA_COLOUR_CYAN = 0x3,
	VGA_COLOUR_RED = 0x4,
	VGA_COLOUR_MAGENTA = 0x5,
	VGA_COLOUR_BROWN = 0x6,
	VGA_COLOUR_GREY = 0x7,
	VGA_COLOUR_DARK_GREY = 0x8,
	VGA_COLOUR_LIGHT_BLUE = 0x9,
	VGA_COLOUR_LIGHT_GREEN = 0xA,
	VGA_COLOUR_LIGHT_CYAN = 0xB,
	VGA_COLOUR_LIGHT_RED = 0xC,
	VGA_COLOUR_LIGHT_MAGENTA = 0xD,
	VGA_COLOUR_YELLOW = 0xE,
	VGA_COLOUR_WHITE = 0xF,
};
 
// initialise first point to manipulate
size_t term_col;
size_t term_row;
uint8_t term_colour;

static inline uint8_t vga_entry_colour(enum vga_colour bg, enum vga_colour fg);
static inline uint16_t vga_entry(uint8_t colour, unsigned char uc);
void term_init();
void term_putc(char c);
void term_print(const char* str);
void kernel_main();

static inline uint8_t vga_entry_colour(enum vga_colour bg, enum vga_colour fg)
{
	return bg << 4 | fg;
}

static inline uint16_t vga_entry(uint8_t colour, unsigned char uc)
{
	return (uint16_t) colour << 8 | (uint16_t) uc;
}
 
// initialising terminal by clearing
void term_init()
{
	term_col = 0;
	term_row = 0;
	term_colour = vga_entry_colour(VGA_COLOUR_BLACK, VGA_COLOUR_GREEN);
	// clear the textmode buffer
	for (size_t row = 0; row < VGA_ROWS; row ++)
	{
		for (size_t col = 0; col < VGA_COLS; col ++)
		{
			// VGA textmode buffer is of size (VGA_COLS * VGA_ROWS)
			const size_t index = (VGA_COLS * row) + col;
			// entries in VGA buffer take the binary form BBBBFFFFCCCCCCCC, 
			// or 0xBFCC, where:
			// - B is the background color
			// - F is the foreground color
			// - C is the ASCII character
			VGA_BUFFER[index] = vga_entry(term_colour, ' '); // Set the character to blank
		}
	}
}
 
// this function places a single character
void term_putc(char c)
{
	// check for newline character
	switch (c)
	{
		case '\n': // newline characters set column to 0, and increment the row
		{
			term_col = 0;
			term_row++;
			break;
		}

		case '\t': // tab character, increment column by 4
		{
			term_col+=4;
			break;
		}
	 
		default: // Normal characters just get displayed and then increment the column
		{
			const size_t index = (VGA_COLS * term_row) + term_col; // Like before, calculate the buffer index
			VGA_BUFFER[index] = vga_entry(term_colour, c);
			term_col++;
			break;
		}
	}
 
	// reset column to 0 and increment row when line is full
	if (term_col >= VGA_COLS)
	{
		term_col = 0;
		term_row ++;
	}
 
	// scroll the terminal when last row is reached
	if (term_row >= VGA_ROWS)
	{
		for(size_t i = 0; i < (VGA_COLS * VGA_ROWS) + VGA_COLS; i++)
		{
			VGA_BUFFER[i] = vga_entry(term_colour, VGA_BUFFER[i + VGA_COLS]);
		}
		term_col = 0;
		term_row--;
	}
}
 
// print string
void term_print(const char* str)
{
	for (size_t i = 0; str[i] != '\0'; i ++) // Keep placing characters until null-terminating character ('\0')
		term_putc(str[i]);
}

void kernel_main()
{
	// Initiate terminal
	term_init();
 
	// Display some messages
	term_print("Hello,\n\t");
	term_colour = vga_entry_colour(VGA_COLOUR_BROWN, VGA_COLOUR_CYAN);
	term_print("World!\n");
}
